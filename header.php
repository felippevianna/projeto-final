<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri() ?>/style.css">
    <title><?php bloginfo('name') ?> <?php wp_title('|'); ?></title>
    <?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>
    <header>
        <div class="header">
            <a href="<?php echo wc_get_page_permalink( 'home' );?>" class="container-img"><img src="<?php echo get_stylesheet_directory_uri() ?>/img/logo.png" alt="logo"></a>
            
            
            <form role="search" method="get" class="woocommerce-product-search" action="<?php echo esc_url( home_url( '/' ) ); ?>">
                <label class="screen-reader-text" for="woocommerce-product-search-field-<?php echo isset( $index ) ? absint( $index ) : 0; ?>"><?php esc_html_e( 'Search for:', 'woocommerce' ); ?></label>
                <img class="lupa" src="<?php echo get_stylesheet_directory_uri() ?>/img/lupa.png" alt="">
                <input type="search" id="woocommerce-product-search-field-<?php echo isset( $index ) ? absint( $index ) : 0; ?>" class="search-field" placeholder="<?php echo esc_attr__( 'Search products&hellip;', 'woocommerce' ); ?>" value="<?php echo get_search_query(); ?>" name="s" />
                <button class="hidden" type="submit" value="<?php echo esc_attr_x( 'Search', 'submit button', 'woocommerce' ); ?>"><?php echo esc_html_x(  'Search', 'submit button', 'woocommerce' ); ?></button>
                <input type="hidden" name="post_type" value="product" />
            </form>
            

            <div class="buttons-header">
                <button id="pedido"><a href="<?php echo wc_get_page_permalink( 'shop' );?>">Faça um pedido</a></button>
                <a id="carrinho"><img src="<?php echo get_stylesheet_directory_uri() ?>/img/carrinho.png" alt=""></a>
                <a href="<?php echo wc_get_page_permalink( 'myaccount' );?>"id="login"><img src="<?php echo get_stylesheet_directory_uri() ?>/img/login.png" alt=""></a>
            </div>
        </div>

        <div class="container-carrinho hidden">
        </div>
        <div class="aparece-carrinho hidden">
            <span id="titulo-carrinho">Carrinho</span>
            <span id="fechar">X</span>
            <hr>
            <?php 
                global $woocommerce;
                $items = $woocommerce->cart->get_cart(); 
                
                
                foreach($items as $item => $values) { ?>
                    <div class="produto-carrinho">
                        <?php $_product =  wc_get_product( $values['data']->get_id()); 
                        echo $_product->get_image(); ?>
                        <div class="infos-produto-carrinho">
                            <p><?php echo $_product->get_title(); ?></p>
                            <div class="container-button-quantity">
                                <button id="diminuir-quantidade">-</button>
                                <input type="text" value="<?php echo $values['quantity'];?>">
                                <button id="aumentar-quantidade">+</button>
                            </div>
                        </div>
                        <?php $price = get_post_meta($values['product_id'] , '_price', true);?>
                        <p class="product-price-carrinho">R$<?php echo $price;?></p>
                    </div>
                    
                <?php } // echo "Link do produto: ".$_product->get_permalink(); ?>
            <hr>
            <p id="mostrar-total">Total do Carrinho: <b><?php echo WC()->cart->get_cart_total(); ?></b></p>
            <button><a id="comprar-carrinho" href="<?php echo wc_get_cart_url(); ?>">Carrinho</a></button>
            <button><a id="comprar-carrinho" href="<?php echo wc_get_checkout_url(); ?>">Comprar</a></button>
        </div>

        <script>
            const carrinho = document.querySelector('#carrinho')
            carrinho.addEventListener('click', (e)=>{
                const apareceCarrinho = document.querySelector('.aparece-carrinho')
                const containerCarrinho = document.querySelector('.container-carrinho')

                apareceCarrinho.classList.toggle("hidden");
                containerCarrinho.classList.toggle("hidden");
            })

            const fecharCarrinho = document.querySelector('#fechar')
            fecharCarrinho.addEventListener('click', (e)=>{
                const apareceCarrinho = document.querySelector('.aparece-carrinho')
                const containerCarrinho = document.querySelector('.container-carrinho')

                apareceCarrinho.classList.toggle("hidden");
                containerCarrinho.classList.toggle("hidden");
            })

        </script>


    </header>
