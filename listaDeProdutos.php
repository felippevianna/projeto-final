<?php 
// Template name: Lista de Produtos
get_header(); ?>

<section class="section-1-lp">
    <div class="categorias-lp">
            <h3>Tipos de pratos principais</h3>
            <div class="menu-categorias-lp">
                <?php 
                    $categorias_final = get_link_category_img();
                    foreach($categorias_final as $category){
                        if($category['name'] != 'Sem categoria'){ ?>
                            <div class="img-categoria-lp" style="background-image:url('<?php echo $category['img']; ?>')">
                                <div class="container-link-lp"><a href="<?php echo $category['link'] ?>"><?php echo $category['name'] ?></a></div>
                            </div>
                        <?php
                        }
                    };
                ?>

                
            </div>
    </div>
</section>


<section class="section-filter">


</section>

<section class="section-produtos">

<?php
    $args=[''];
    $products = wc_get_products($args);
    $product_formatado = format_products($products);
    foreach($product_formatado as $product) { ?>
        <div class="prato-lp" style="background-image:url('<?php echo $product['img_url'] ?>')">   
            <div class="container-infos-lp">
                <p class="nome-prato"><?php echo $product['name']; ?></p>
                <p class="preco-prato">R$<?php echo $product['price']; ?></p>
                <a href="<?php echo $product['link_prod'] ?>" class="adicionar-carrinho"><img src="<?php echo get_stylesheet_directory_uri() ?>/img/adicionar-carrinho.png" alt=""></a>
            </div>
        </div>
<?php }; ?>

</section>



<?php
get_footer();
?>