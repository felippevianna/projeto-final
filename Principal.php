<?php 
// Template name: Principal
get_header(); ?>

<section class="section-1-home">
    <h1>Comes&Bebes</h1>
    <h2>O restaurante para todas as fomes</h2>
</section>

<section class="section-2-home">
    <h1>Conheça a nossa loja</h1>

    <div class="categorias">
        <h3>Tipos de pratos principais</h3>
        <div class="menu-categorias">
        <?php 
            $categorias_final = get_link_category_img();
            foreach($categorias_final as $category){
                if($category['name'] != 'Sem categoria'){ ?>
                    <div class="img-categoria" style="background-image:url('<?php echo $category['img']; ?>')">
                        <div class="container-link"><a href="<?php echo $category['link'] ?>"><?php echo $category['name'] ?></a></div>
                    </div>
                <?php
                }
            };
        ?>
        </div>
    </div>

    <div class="pratos-dia">
        <h3>Pratos do dia de hoje:</h3>
        <p><?php
    
            date_default_timezone_set("America/Sao_Paulo");
            $diasemana = date('l');
    
            switch ($diasemana) {
                case 'Monday':
                    echo 'SEGUNDA';
                    break;
                case 'Tuesday':
                    echo 'TERÇA';
                    break;
                case 'Wednesday':
                    echo 'QUARTA';
                    break;
                case 'Thursday':
                    echo 'QUINTA';
                    break;
                case 'Friday':
                    echo 'SEXTA';
                    break;
                case 'Saturday':
                    echo 'SÁBADO';
                    break;
                case 'Sunday':
                    echo 'DOMINGO';
                    break;
                default:
                    echo 'O mundo acabou, pois hoje nao é dia da semana';
            }   
        ?></p>
        <div class="pratos">
            <?php
                date_default_timezone_set("America/Sao_Paulo");
                $products = wc_get_products([
                    'limit'=> 4,
                    'tag'=> date('l'),
                ]);
    
                $products_formatado = format_products($products);
                    
                // print_r($products_formatado[0]);
    
                foreach($products_formatado as $product) { ?>
                    <div class="prato-atual" style="background-image:url('<?php echo $product['img_url']; ?>')">
                        <div class="container-infos">
                            <p class="nome-do-dia"><?php echo $product['name']; ?></p>
                            <p class="preco-do-dia">R$<?php echo $product['price']; ?></p>
                            <a href="<?php echo $product['link_prod'] ?>" class="adicionar-carrinho"><img src="<?php echo get_stylesheet_directory_uri() ?>/img/adicionar-carrinho.png" alt=""></a>
                        </div>
                    </div>
                <?php } ?>
        </div>
    </div>

    <button><a href="http://comesbebes.local/lista-de-produtos/">Veja outras opções</a></button>
</section>

<section class="section-3-home">
    <h2>Visite nossa loja física</h2>
    <div class="container-mapa-slider">
        <div class="mapa">
            <div class="mapouter"><div class="gmap_canvas"><iframe width="400" height="270" id="gmap_canvas" src="https://maps.google.com/maps?q=Rua%20Basileu%20Nogueira%20da%20costa&t=&z=13&ie=UTF8&iwloc=&output=embed" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"></iframe><a href="https://123movies-to.org">123 movies</a><br><style>.mapouter{position:relative;text-align:right;height:auto;width:100%;}</style><a href="https://www.embedgooglemap.net">google maps embed api</a><style>.gmap_canvas {overflow:hidden;background:none!important;height:270px;width:400px;}</style></div></div>

            <p><img src="<?php echo get_stylesheet_directory_uri() ?>/img/endereco.png" alt="">   Rua Basileu Nogueira da Costa</p>
            <p><img src="<?php echo get_stylesheet_directory_uri() ?>/img/telefone.png" alt="">   (XX)XXXX-XXXX</p>
        </div>

        <div class="slider">
            <img src="<?php echo get_stylesheet_directory_uri() ?>/img/img1slider.png" alt="">
        </div>
    </div>
</section>

<?php
get_footer();
?>
