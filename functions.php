<?php
    function mytheme_add_woocommerce_support() {
        add_theme_support( 'woocommerce' );
    }
    add_action( 'after_setup_theme', 'mytheme_add_woocommerce_support' );

    function format_products($products){
        $products_final = [];
        foreach($products as $product) {
            $image_id  = $product->get_image_id();
            $image_url = wp_get_attachment_image_src( $image_id, 'slide' )[0];
            $products_final[] = [
                'name' => $product->get_name(),
                'price'=>$product->get_price(),
                'link_prod'=>$product->get_permalink(),
                'img'=>$product->get_image(),
                'img_url'=>$image_url,
            ];
        };

        return $products_final;
    };


    // função de pegar fotos das categorias
    function get_link_category_img(){
        $taxonomy     = 'product_cat';
        $orderby      = 'name';  
        $show_count   = 0;      // 1 for yes, 0 for no
        $pad_counts   = 0;      // 1 for yes, 0 for no
        $hierarchical = 1;      // 1 for yes, 0 for no  
        $title        = '';  
        $empty        = 0;
      
        $args = array(
               'taxonomy'     => $taxonomy,
               'orderby'      => $orderby,
               'show_count'   => $show_count,
               'pad_counts'   => $pad_counts,
               'hierarchical' => $hierarchical,
               'title_li'     => $title,
               'hide_empty'   => $empty
        );
        $categorias_lista = [];
        $categories = get_categories($args);
        if ($categories){
            foreach($categories as $category){
                $category_id = $category->term_id;
                $img_id = get_term_meta($category_id, 'thumbnail_id', true);
                $categorias_lista[] = [
                    'name' => $category->name,
                    'id'=> $category_id,
                    'link'=> get_term_link($category_id, 'product_cat'),
                    'img'=> wp_get_attachment_image_src($img_id, 'slide')[0],
                ];

            };
        };
        return $categorias_lista;
    };

    // fim da função de pegar fotos


    function add_categories(){ ?>
        <section class="section-2-home">
            <div class="categorias">
                    <h3>Tipos de pratos principais</h3>
                    <div class="menu-categorias">
                        <?php 
                            $categorias_final = get_link_category_img();
                            foreach($categorias_final as $category){
                                if($category['name'] != 'Sem categoria'){ ?>
                                    <div class="img-categoria" style="background-image:url('<?php echo $category['img']; ?>')">
                                        <div class="container-link"><a href="<?php echo $category['link'] ?>"><?php echo $category['name'] ?></a></div>
                                    </div>
                                <?php
                                }
                            };
                        ?>

                        
                    </div>
            </div>
        </section>
        <?php
    };
    add_action('woocommerce_before_shop_loop', 'add_categories');

    function add_search_form(){?>

        <form role="search" method="get" class="woocommerce-product-search" action="<?php echo esc_url( home_url( '/' ) ); ?>">
            <label class="screen-reader-text" for="woocommerce-product-search-field-<?php echo isset( $index ) ? absint( $index ) : 0; ?>"><?php esc_html_e( 'Search for:', 'woocommerce' ); ?></label>
            <input type="search" id="woocommerce-product-search-field-<?php echo isset( $index ) ? absint( $index ) : 0; ?>" class="search-field" placeholder="<?php echo esc_attr__( 'Search products&hellip;', 'woocommerce' ); ?>" value="<?php echo get_search_query(); ?>" name="s" />
            <button type="submit" value="<?php echo esc_attr_x( 'Search', 'submit button', 'woocommerce' ); ?>"><?php echo esc_html_x( 'Search', 'submit button', 'woocommerce' ); ?></button>
            <input type="hidden" name="post_type" value="product" />
        </form>
    <?php
    };
    add_action('woocommerce_before_shop_loop','add_search_form');


    function price_filter(){?>
            <?php do_action( 'woocommerce_widget_price_filter_start', $args ); ?>

            <form method="get" action="<?php echo esc_url( $form_action ); ?>">
                <div class="price_slider_wrapper">
                    <div class="price_slider" style="display:none;"></div>
                    <div class="price_slider_amount" data-step="<?php echo esc_attr( $step ); ?>">
                        <input type="text" id="min_price" name="min_price" value="<?php echo esc_attr( $current_min_price ); ?>" data-min="<?php echo esc_attr( $min_price ); ?>" placeholder="<?php echo esc_attr__( 'Min price', 'woocommerce' ); ?>" />
                        <input type="text" id="max_price" name="max_price" value="<?php echo esc_attr( $current_max_price ); ?>" data-max="<?php echo esc_attr( $max_price ); ?>" placeholder="<?php echo esc_attr__( 'Max price', 'woocommerce' ); ?>" />
                        <?php /* translators: Filter: verb "to filter" */ ?>
                        <button type="submit" class="button"><?php echo esc_html__( 'Filter', 'woocommerce' ); ?></button>
                        <div class="price_label" style="display:none;">
                            <?php echo esc_html__( 'Price:', 'woocommerce' ); ?> <span class="from"></span> &mdash; <span class="to"></span>
                        </div>
                        <?php echo wc_query_string_form_fields( null, array( 'min_price', 'max_price', 'paged' ), '', true ); ?>
                        <div class="clear"></div>
                    </div>
                </div>
            </form>

            <?php do_action( 'woocommerce_widget_price_filter_end', $args ); ?>
        <?php    
        }
        add_action('woocommerce_before_shop_loop','price_filter');


        // Funções de remoção ========================================================================

        remove_action('woocommerce_single_product_summary','woocommerce_template_single_excerpt',20);
            function woocommerce_template_single_excerpt(){
        };

        remove_action('woocommerce_single_product_summary','woocommerce_template_single_meta',40);
            function remove_woocommerce_template_single_meta(){
        };

        remove_action('woocommerce_single_product_summary','woocommerce_template_single_title',5);
            function remove_woocommerce_template_single_title(){
        };

        remove_action('woocommerce_after_single_product_summary','woocommerce_output_product_data_tabs',10);
            function remove_woocommerce_output_product_data_tabs(){
        };




?>